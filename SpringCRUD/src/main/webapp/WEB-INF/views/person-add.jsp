<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div align="center">

		<h1>New Person</h1>
		<form:form action="savePerson" method="post">
			<table>
				<tr>
					<td>ID Person :</td>
					<td><input type="number" name="idPerson" required="required" /></td>
				</tr>

				<tr>
					<td>Nama Person :</td>
					<td><input type="text" name="namaPerson" required="required" /></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit"
						value="Save"></td>
				</tr>
			</table>
		</form:form>

	</div>

</body>
</html>