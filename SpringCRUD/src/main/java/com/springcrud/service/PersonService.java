package com.springcrud.service;

import com.springcrud.model.Person;

public interface PersonService extends GenericInterfaceService<Person, Long> {

}
