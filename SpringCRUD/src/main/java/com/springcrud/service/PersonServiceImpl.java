package com.springcrud.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springcrud.dao.PersonDao;
import com.springcrud.model.Person;

@Transactional
@Service
public class PersonServiceImpl implements PersonService{
	
	@Autowired
	PersonDao personDao;

	public List<Person> getAll() {
		List<Person> orang = null;
		try {

			orang = new ArrayList<Person>();
			orang = personDao.getAll();

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
		}

		return orang;
	}

	public Person getById(Long id) {
		Person orang = null;
		
		try {
			orang = personDao.getById(id);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return orang;
	}

	public void delete(Long id) {
		Person orang= null;
		try {
			orang = new Person();
			orang = this.getById(id);
			if(orang!=null)
			{
				personDao.delete(orang);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
		}
	}

	public void update(Person entity) {
		try {
			if(entity.getNamaPerson()!=null)
			{
				personDao.update(entity);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
		}
	}

	public void save(Person entity) {
		try {	
			if (entity.getNamaPerson() != null) {
				System.out.println("MASUK SAVE SERVICE");
				personDao.save(entity);
				System.out.println("SUDAH SAVE SERVICE");

			}
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
		}
	}

}
