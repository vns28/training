package com.springcrud.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.springcrud.model.Person;
import com.springcrud.service.PersonService;

@Controller
public class PersonController {

	@Autowired
	PersonService ps;
	
	@RequestMapping(value = "/addPerson")
	public ModelAndView addPerson(Model model) {
		ModelAndView modelAndView = new ModelAndView("person-add");

		try {
			Person orang = new Person();
			
			modelAndView.addObject("orang", orang);

		} catch (Exception e) {
			System.err.println("Error Add Person");
		}

		return modelAndView;
	}
	
	@RequestMapping(value = "/savePerson", method = RequestMethod.POST)
	public ModelAndView saving(@ModelAttribute Person orang) {
		ModelAndView modelAndView = new ModelAndView("person-list");
		
		try {

			if (orang != null) {

				ps.save(orang);			

				modelAndView.addObject("orang", orang);

				System.err.println("after save");
			} else {
				modelAndView.addObject("message", "Data tidak boleh kosong");
			}

		} catch (Exception e) {
			// TODO: handle exception

			System.err.println("controller /savePerson: " + e);
		}

		return new ModelAndView("redirect:/person-list");
	}
	
	@RequestMapping(value = "/person-list")
	public ModelAndView getAllData() {

		ModelAndView modelAndView = null;
		List<Person> orang = null;
		try {
			orang = new ArrayList<Person>();
			modelAndView = new ModelAndView("person-list");

			orang = ps.getAll();

			modelAndView.addObject("listPerson", orang);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}

		return modelAndView;
	}
	
	@RequestMapping(value = "/person-delete/{id}", method = RequestMethod.GET)
	public ModelAndView deletePerson(@PathVariable(value = "id") Long id) {

		try {
			
			ps.delete(id);

		} catch (Exception e) {
			System.err.println("Error Delete Person" + e);
		}

		return new ModelAndView("redirect:/person-list");
	}
	
	@RequestMapping(value = "/person-edit/{id}")
	public ModelAndView editPerson(@PathVariable(value = "id") Long id) {

		ModelAndView modelAndView = new ModelAndView("person-edit");
		Person orang = null;
		try {

			orang = new Person();
			orang = ps.getById(id);
			modelAndView.addObject("orang", orang);

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("error reqMap controller person-edit " + e);
		}
		
		return modelAndView;
	}
	
	@RequestMapping(value = "/person-update", method = RequestMethod.POST)
	public ModelAndView updateBarang(@ModelAttribute Person orang) {

		ModelAndView modelAndView = new ModelAndView("person-edit");
		try {

			if (orang != null) {
				ps.update(orang);
			}
			modelAndView.addObject("Sukses", "Data berikut berhasil di update");
			modelAndView.addObject("orang", orang);

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("controller / updatePerson:  " + e);
		}

		return new ModelAndView("redirect:/person-list");
	}
}
