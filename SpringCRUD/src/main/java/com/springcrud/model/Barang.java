package com.springcrud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table (name = "barang")
public class Barang {
	@Id
	private long idBarang;
	
	@Column
	private String namaBarang;
	
	@Column
	private double hargaBarang;
	
	public double getHargaBarang() {
		return hargaBarang;
	}

	public void setHargaBarang(double hargaBarang) {
		this.hargaBarang = hargaBarang;
	}

	@ManyToOne
	@JoinColumn(name="idPerson", referencedColumnName="idPerson", insertable = true, updatable = true)
	private Person person;
	
	@Transient
	private long idPerson;

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public long getIdPerson() {
		return idPerson;
	}

	public void setIdPerson(long idPerson) {
		this.idPerson = idPerson;
	}

	public long getIdBarang() {
		return idBarang;
	}

	public void setIdBarang(long idBarang) {
		this.idBarang = idBarang;
	}

	public String getNamaBarang() {
		return namaBarang;
	}

	public void setNamaBarang(String namaBarang) {
		this.namaBarang = namaBarang;
	}
	
	
}
