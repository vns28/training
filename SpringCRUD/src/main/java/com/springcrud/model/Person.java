package com.springcrud.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "Person")
public class Person {
	@Id
	public long idPerson;
	
	@Column
	public String namaPerson;
	
	@OneToMany (mappedBy = "person", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Barang> bar;

	public Set<Barang> getBar() {
		return bar;
	}

	public void setBar(Set<Barang> bar) {
		this.bar = bar;
	}

	public long getIdPerson() {
		return idPerson;
	}

	public void setIdPerson(long idPerson) {
		this.idPerson = idPerson;
	}

	public String getNamaPerson() {
		return namaPerson;
	}

	public void setNamaPerson(String namaPerson) {
		this.namaPerson = namaPerson;
	}

	
	
}
