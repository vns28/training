package com.springcrud.dao;

import com.springcrud.model.Person;

public interface PersonDao extends GenericInterfaceDao<Person, Long>{

}
