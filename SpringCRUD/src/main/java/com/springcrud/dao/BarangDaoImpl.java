package com.springcrud.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.springcrud.model.Barang;

@Repository
public class BarangDaoImpl extends AbstractDao implements BarangDao{
	@SuppressWarnings("unchecked")
	public List<Barang> getAll() {
		// TODO Auto-generated method stub
		List<Barang> item = null;
		
		String sql = "From Barang";
		
		try {			
			item = new ArrayList<Barang>();
			getSession().beginTransaction();
			Query query = getSession().createQuery(sql);
			item = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
		}finally {
			getSession().close();
		}
		
		return item;
	}

	public Barang getById(Long id) {
		// TODO Auto-generated method stub
		Barang item = null;
		try {
			item = new Barang();
			getSession().beginTransaction();
			item =   (Barang) getSession().get(Barang.class, id);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("getById barangDaoImpl :" +e);
		}finally {
			getSession().getTransaction().commit();
			getSession().close();
		}
		
		return item;
	}

	public void delete(Barang entity) {
		// TODO Auto-generated method stub
		try {
			getSession().beginTransaction();
			getSession().delete(entity);
		
		
			
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
			getSession().getTransaction().rollback();
		}finally {
			getSession().getTransaction().commit();
			getSession().close();
		}
		
	}

	public void update(Barang update) {
		// TODO Auto-generated method stub
		
		try {
			getSession().beginTransaction();
			getSession().update(update);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
			getSession().getTransaction().rollback();
		}finally {
			getSession().getTransaction().commit();
			getSession().close();
		}
		
	}

	public void save(Barang entity) {
		// TODO Auto-generated method stub
		
		try {			
			getSession().beginTransaction();
			getSession().save(entity);
		} catch (Exception e) {
			// TODO: handle exception
			
			System.err.println(e);
			getSession().getTransaction().rollback();
			
		}finally {
			getSession().getTransaction().commit();
			getSession().close();
		}
	}
}

