package com.latihan.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PersonController {
	@RequestMapping(value = "/index0")
	public String passParametersWithModelMap(ModelMap map) {
		map.addAttribute("welcomeMessage", "welcome");
		map.addAttribute("message","Baeldung");
		return "index";
	}
	
	@GetMapping("/index1")
	public String passParametersWithModel(Model model) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("spring", "mvc"); //lebih sering dipake di model
		model.addAttribute("pesan","Model"); //sering dipake di model dan modelmap
		model.mergeAttributes(map);
		return "index";
	}
	
	@GetMapping("/index2")
	public ModelAndView passParametersWithModelAndView() {
	    ModelAndView modelAndView = new ModelAndView("index");
	    modelAndView.addObject("kabar", "this");
	    modelAndView.addObject("msg", "is training");
	    return modelAndView;
	}
	
	@RequestMapping(value = "/helloworld")
	public ModelAndView update(ModelMap map, Model model) {
		model.addAttribute("hel","Model");
		map.put("hell","Model Map");
		ModelAndView modelAndView = new ModelAndView ("hello");
		modelAndView.addObject("he","ModelAndView");
		
		return modelAndView;
	}
}