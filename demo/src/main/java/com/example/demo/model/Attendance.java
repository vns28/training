/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

//import org.springframework.boot.autoconfigure.domain.EntityScan;

@Entity
@Table(name = "attendance")
@NamedQueries({
    @NamedQuery(name = "Attendance.findAll", query = "SELECT a FROM Attendance a")})
public class Attendance implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "atten_id")
    private Integer attenId;
    @Column(name = "Emp_ID")
    private Integer empID;
    @Column(name = "Att_Date")
    @Temporal(TemporalType.DATE)
    private Date attDate;
    @Size(max = 20)
    @Column(name = "Att_Status")
    private String attStatus;

    public Attendance() {
    }

    public Attendance(Integer attenId) {
        this.attenId = attenId;
    }

    public Integer getAttenId() {
        return attenId;
    }

    public void setAttenId(Integer attenId) {
        this.attenId = attenId;
    }

    public Integer getEmpID() {
        return empID;
    }

    public void setEmpID(Integer empID) {
        this.empID = empID;
    }

    public Date getAttDate() {
        return attDate;
    }

    public void setAttDate(Date attDate) {
        this.attDate = attDate;
    }

    public String getAttStatus() {
        return attStatus;
    }

    public void setAttStatus(String attStatus) {
        this.attStatus = attStatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (attenId != null ? attenId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Attendance)) {
            return false;
        }
        Attendance other = (Attendance) object;
        if ((this.attenId == null && other.attenId != null) || (this.attenId != null && !this.attenId.equals(other.attenId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.example.demo.entity.Attendance[ attenId=" + attenId + " ]";
    }
    
}
