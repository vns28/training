package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;
import java.util.List;

import com.example.demo.model.Employees;

public interface EmployeesRepo extends CrudRepository<Employees, Integer> {
	public Employees findByEmpID(Integer empId);
	public List<Employees> findByPosID(Integer posId);
	public List<Employees> findByEmpName(String empName);
	public List<Employees> findByGender(String Gender);
	public List<Employees> findByAge(Integer Age);
	public List<Employees> findByAgeAndGender(Integer Age, String Gender);
	public List<Employees> findByAgeAndGenderAndPosID(Integer Age, String Gender, Integer posId);
}
