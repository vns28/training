package com.example.demo.service;

import java.util.List;
import java.util.Map;

import com.example.demo.model.Employees;

public interface EmployeeService {
	public Employees findByEmpID(Integer empId);
	public List<Employees> findAll();
	public List<Employees> findByPosID(Integer posId);
	public List<Employees> findByEmpName(String empName);
	public List<Employees> findByGender(String Gender);
	public List<Employees> findByAge(Integer Age);
	public Map<String, String> setEmp(Employees e);
	public Map<String, String> delEmp(Employees e);
	public List<Employees> findByAgeAndGender(Integer Age, String Gender);
	public List<Employees> findByAgeAndGenderAndPosID(Integer Age, String Gender, Integer posId);
}
