package com.example.demo.service;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import com.example.demo.model.Attendance;

public interface AttendanceService {
	public List<Attendance> findAll();
	public Attendance findByAttenId(Integer attenId);
	public List<Attendance> findByEmpID(Integer empId);
	public List<Attendance> findByAttStatus(String attStat);
	public List<Attendance> findByAttDate(Date attdate);
	public Map<String, String> setAtten(Attendance a);
	public Map<String, String> delAtten(Attendance a);
}
