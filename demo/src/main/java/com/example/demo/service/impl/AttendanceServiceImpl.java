package com.example.demo.service.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.Attendance;
import com.example.demo.repository.AttendanceRepo;
import com.example.demo.service.AttendanceService;

@Service
@Transactional
public class AttendanceServiceImpl implements AttendanceService{
	@Autowired
	public AttendanceRepo ar;
	
	@Override
	public List<Attendance> findAll() {
		List<Attendance> att = new ArrayList<Attendance>();
		att = (List<Attendance>) ar.findAll();
		
		return att;
	}

	@Override
	public Attendance findByAttenId(Integer attenId) {
		Attendance att = new Attendance();
		att = ar.findByAttenId(attenId);
		
		return att;
	}

	@Override
	public List<Attendance> findByEmpID(Integer empId) {
		List<Attendance> att = new ArrayList<Attendance>();
		att = (List<Attendance>) ar.findByEmpID(empId);
		
		return att;
	}

	@Override
	public List<Attendance> findByAttStatus(String attStat) {
		List<Attendance> att = new ArrayList<Attendance>();
		att = (List<Attendance>) ar.findByAttStatus(attStat);
		
		return att;
	}

	@Override
	public List<Attendance> findByAttDate(Date attdate) {
		List<Attendance> att = new ArrayList<Attendance>();
		att = (List<Attendance>) ar.findByAttDate(attdate);
		
		return att;
	}
	
	public Map<String, String> setAtten(Attendance a){
		Map<String, String> resMap = new LinkedHashMap<String, String>();
		try {
			a = ar.save(a);
			resMap.put("attenID", a.getAttenId().toString());
			resMap.put("code", HttpStatus.OK+"");
			resMap.put("msg", "success");
		} catch (Exception e2) {
			// TODO: handle exception
			
			resMap.put("attenID", "");
			resMap.put("code", HttpStatus.CONFLICT+"");
			resMap.put("msg", "fail");
		}
		
		return resMap;
	}
	
	public Map<String, String> delAtten(Attendance a){
		Map<String, String> resMap = new LinkedHashMap<String, String>();
		try {
			ar.delete(a);
			resMap.put("attenID", a.getAttenId().toString());
			resMap.put("code", HttpStatus.OK+"");
			resMap.put("msg", "success");
		} catch (Exception e2) {
			// TODO: handle exception
			
			resMap.put("attenID", "");
			resMap.put("code", HttpStatus.CONFLICT+"");
			resMap.put("msg", "fail");
		}
		
		return resMap;
	}

}
