package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.Employees;
import com.example.demo.repository.EmployeesRepo;
import com.example.demo.service.EmployeeService;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService{
	@Autowired
	private EmployeesRepo er;
	
	public Employees findByEmpID(Integer empId) {
		// TODO Auto-generated method stub
		Employees employees = new Employees();
		employees = er.findByEmpID(empId);
		
		return employees;
	}

	public List<Employees> findAll() {
		// TODO Auto-generated method stub
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findAll();
		
		return empls;
	}

	public List<Employees> findByPosID(Integer posId) {
		// TODO Auto-generated method stub
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findByPosID(posId);
		
		return empls;
	}

	public List<Employees> findByEmpName(String empName) {
		// TODO Auto-generated method stub
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findByEmpName(empName);
		
		return empls;
	}

	public List<Employees> findByGender(String Gender) {
		// TODO Auto-generated method stub
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findByGender(Gender);
		
		return empls;
	}

	public List<Employees> findByAge(Integer Age) {
		// TODO Auto-generated method stub
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findByAge(Age);
		
		return empls;
	}

	public Map<String, String> setEmp(Employees e) {
		// TODO Auto-generated method stub
		Map<String, String> resMap = new LinkedHashMap<String, String>();
		try {
			e = er.save(e);
			resMap.put("empID", e.getEmpID().toString());
			resMap.put("code", HttpStatus.OK+"");
			resMap.put("msg", "success");
		} catch (Exception e2) {
			// TODO: handle exception
			
			resMap.put("empID", "");
			resMap.put("code", HttpStatus.CONFLICT+"");
			resMap.put("msg", "fail");
		}
		
		return resMap;
	}

	public Map<String, String> delEmp(Employees e) {
		// TODO Auto-generated method stub
		Map<String, String> resMap = new LinkedHashMap<String, String>();
		try {
			er.delete(e);
			resMap.put("empID", e.getEmpID().toString());
			resMap.put("code", HttpStatus.OK+"");
			resMap.put("msg", "success");
		} catch (Exception e2) {
			// TODO: handle exception
			
			resMap.put("empID", "");
			resMap.put("code", HttpStatus.CONFLICT+"");
			resMap.put("msg", "fail");
		}
		
		return resMap;
	}
	
	public List<Employees> findByAgeAndGender(Integer Age, String Gender){
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findByAgeAndGender(Age, Gender);
		
		return empls;
	}
	
	public List<Employees> findByAgeAndGenderAndPosID(Integer Age, String Gender, Integer posId){
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findByAgeAndGenderAndPosID(Age, Gender, posId);
		
		return empls;
	}
	
}
