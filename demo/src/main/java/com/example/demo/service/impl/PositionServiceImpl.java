package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.Position;
import com.example.demo.repository.PositionRepo;
import com.example.demo.service.PositionService;

@Service
@Transactional
public class PositionServiceImpl implements PositionService{
	@Autowired
	private PositionRepo pr;
	
	@Override
	public Position findByPosID(Integer posId) {
		Position pos = new Position();
		pos = pr.findByPosID(posId);
		
		return pos;
	}

	@Override
	public List<Position> findAll() {
		List<Position> pos = new ArrayList<Position>();
		pos = (List<Position>) pr.findAll();
		
		return pos;
	}

	@Override
	public Position findByPosName(String posName) {
		Position pos = new Position();
		pos = pr.findByPosName(posName);
		
		return pos;
	}

	@Override
	public List<Position> findByAllowance(Integer allowance) {
		List<Position> pos = new ArrayList<Position>();
		pos = (List<Position>) pr.findByAllowance(allowance);
		
		return pos;
	}
	
	public Map<String, String> setPos(Position p){
		Map<String, String> resMap = new LinkedHashMap<String, String>();
		try {
			p = pr.save(p);
			resMap.put("posID", p.getPosID().toString());
			resMap.put("code", HttpStatus.OK+"");
			resMap.put("msg", "success");
		} catch (Exception e2) {
			// TODO: handle exception
			
			resMap.put("posID", "");
			resMap.put("code", HttpStatus.CONFLICT+"");
			resMap.put("msg", "fail");
		}
		
		return resMap;
	}
	
	public Map<String, String> delPos(Position p){
		Map<String, String> resMap = new LinkedHashMap<String, String>();
		try {
			pr.delete(p);
			resMap.put("posID", p.getPosID().toString());
			resMap.put("code", HttpStatus.OK+"");
			resMap.put("msg", "success");
		} catch (Exception e2) {
			// TODO: handle exception
			
			resMap.put("posID", "");
			resMap.put("code", HttpStatus.CONFLICT+"");
			resMap.put("msg", "fail");
		}
		
		return resMap;
	}

}
