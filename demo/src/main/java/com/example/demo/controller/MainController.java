package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Attendance;
import com.example.demo.model.Employees;
import com.example.demo.model.Position;
import com.example.demo.service.AttendanceService;
import com.example.demo.service.EmployeeService;
import com.example.demo.service.PositionService;

import java.util.List;
import java.util.Map;
import java.sql.Date;

@RestController
public class MainController {
	//http://localhost:8080/getempid?empId=5
	
	@Autowired
	private EmployeeService es;
	
	//get one row of data from table employees by employee ID
	@GetMapping("/getempid")
	public Employees getByEmpId(@RequestParam Integer empId) {
		return es.findByEmpID(empId);
	}
	
	//get every data in employees
	@GetMapping("/getempall")
	public List<Employees> getEmpAll(){
		return es.findAll();
	}
	
	//get list of data in employees by the posid
	@GetMapping("/getemppos")
	public List<Employees> getByPosId(@RequestParam Integer posId) {
		return es.findByPosID(posId);
	}
	
	@GetMapping("/getempname")
	public List<Employees> getByEmpName(@RequestParam String empName) {
		return es.findByEmpName(empName);
	}
	
	@GetMapping("/getempgender")
	public List<Employees> getByGender(@RequestParam String Gender) {
		return es.findByGender(Gender);
	}
	
	@GetMapping("/getempage")
	public List<Employees> getByAge(@RequestParam Integer Age) {
		return es.findByAge(Age);
	}
	
	//http://localhost:8080/getageandgender?Age=19&Gender=M
	@GetMapping("/getageandgender")
	public List<Employees> getByAgeandGender(@RequestParam Integer Age, @RequestParam String Gender){
		return es.findByAgeAndGender(Age, Gender);
	}
	
	@GetMapping("/getageandgenderandposid")
	public List<Employees> getByAgeandGenderandPosId(@RequestParam Integer Age, @RequestParam String Gender, @RequestParam Integer PosId){
		return es.findByAgeAndGenderAndPosID(Age, Gender, PosId);
	}
	
	//add/update an object into the db
	//http://localhost:8080/setemp; click 'body' tab -> change to raw and json -> write the json object in the given textbox
	//the json object ex: {"empName":"Sultan","posID":1,"gender":"M","age":19}
	@PostMapping("/setemp")
	public Map<String, String> setEmp(@RequestBody Employees e) {
		return setEmp(e);
	}
	
	//delete entry from employee
	@PostMapping("/deleteemp")
	public Map<String, String> delEmp(@RequestBody Employees e) {
		return es.delEmp(e);
	}
		
	/*--------------------------------Position--------------------------------*/
	@Autowired
	private PositionService ps;
	
	@GetMapping("/getposall")
	public List<Position> getPosAll(){
		return ps.findAll();
	}
	
	@GetMapping("/getposid")
	public Position getPosByPosId(@RequestParam Integer posId) {
		return ps.findByPosID(posId);
	}
	
	@GetMapping("/getposname")
	public Position getByPosName(@RequestParam String posName) {
		return ps.findByPosName(posName);
	}
	
	@GetMapping("/getallowance")
	public List<Position> getByAllowance(@RequestParam Integer allowance) {
		return ps.findByAllowance(allowance);
	}
	
	@PostMapping("/setpos")
	public Map<String, String> setPos(@RequestBody Position p) {
		return ps.setPos(p);
	}
	
	@PostMapping("/deletepos")
	public Map<String, String> delPos(@RequestBody Position p) {
		return ps.delPos(p);
	}
	
	/*--------------------------------Attendance--------------------------------*/
	@Autowired
	private AttendanceService as;
	
	@GetMapping("/getattenall")
	public List<Attendance> getAttenAll(){
		return as.findAll();
	}
	
	@GetMapping("/getattenid")
	public Attendance getByAttenId(@RequestParam Integer attenId) {
		return as.findByAttenId(attenId);
	}
	
	@GetMapping("/getattenbyempid")
	public List<Attendance> getAttenByEmpId(@RequestParam Integer empId) {
		return as.findByEmpID(empId);
	}
	
	@GetMapping("/getbystatus")
	public List<Attendance> getByAttenStatus(@RequestParam String attStat) {
		return as.findByAttStatus(attStat);
	}
	
	@GetMapping("/getbydate")
	public List<Attendance> getByAttenDate(@RequestParam Date attdate) {
		return as.findByAttDate(attdate);
	}
	
	@PostMapping("/setatten")
	public Map<String, String> setAtten(@RequestBody Attendance a) {
		return as.setAtten(a);
	}
	
	@PostMapping("/deleteatten")
	public Map<String, String> delAtten(@RequestBody Attendance a) {
		return as.delAtten(a);
	}
}