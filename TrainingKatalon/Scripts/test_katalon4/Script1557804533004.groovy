import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.jakmall.com/')

WebUI.click(findTestObject('Object Repository/Page_Jakmallcom  Jual Beli Online Mudah dan Aman/b_Login'))

WebUI.setText(findTestObject('Object Repository/Page_Yuk Login ke Jakmallcom/input_Email_email'), 'hnbeetry@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Yuk Login ke Jakmallcom/input_Lupa Password_password'), 'iJryLYPb6LiUUPnlruGOvA==')

WebUI.click(findTestObject('Object Repository/Page_Yuk Login ke Jakmallcom/input_Ingat login saya_button button--cta button--md button--full'))

result = WebUI.getText(findTestObject('Page_Jakmallcom  Jual Beli Online Mudah dan Aman/div_Vanessa'))

WebUI.verifyMatch(result, 'Vanessa', false)

WebUI.closeBrowser()

