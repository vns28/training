
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String

import com.kms.katalon.core.testobject.TestObject


def static "database.MyLDAPConnection.connectLDAP"(
    	String host	
     , 	int port	) {
    (new database.MyLDAPConnection()).connectLDAP(
        	host
         , 	port)
}

def static "database.MyLDAPConnection.authentication"(
    	String uid	
     , 	String password	) {
    (new database.MyLDAPConnection()).authentication(
        	uid
         , 	password)
}

def static "database.MyLDAPConnection.checkUser"(
    	String uid	) {
    (new database.MyLDAPConnection()).checkUser(
        	uid)
}

def static "database.MyLDAPConnection.closeLDAP"() {
    (new database.MyLDAPConnection()).closeLDAP()
}

def static "get.Screencapture.getElement"(
    	TestObject object	
     , 	String fileName	) {
    (new get.Screencapture()).getElement(
        	object
         , 	fileName)
}

def static "get.Screencapture.getEntirePage"(
    	String fileName	) {
    (new get.Screencapture()).getEntirePage(
        	fileName)
}

def static "get.Screencapture.getEntirePageMobile"(
    	String fileName	) {
    (new get.Screencapture()).getEntirePageMobile(
        	fileName)
}

def static "set.ElementApr.addAttribute"(
    	TestObject to	
     , 	String attr	
     , 	String value	) {
    (new set.ElementApr()).addAttribute(
        	to
         , 	attr
         , 	value)
}

def static "database.MyConnection.connectDB"(
    	String database	
     , 	String url	
     , 	String dbname	
     , 	String port	
     , 	String username	
     , 	String password	) {
    (new database.MyConnection()).connectDB(
        	database
         , 	url
         , 	dbname
         , 	port
         , 	username
         , 	password)
}

def static "database.MyConnection.executeQuery"(
    	String queryString	) {
    (new database.MyConnection()).executeQuery(
        	queryString)
}

def static "database.MyConnection.closeDatabaseConnection"() {
    (new database.MyConnection()).closeDatabaseConnection()
}

def static "database.MyConnection.execute"(
    	String queryString	) {
    (new database.MyConnection()).execute(
        	queryString)
}
